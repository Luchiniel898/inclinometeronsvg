import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import router from './router'
import 'leaflet/dist/leaflet.css';
import { LMap, LTileLayer, LMarker, LPopup, LTooltip, LCircle, LCircleMarker, LControlScale, LIcon } from 'vue2-leaflet';
import ajaxpost from "./ajaxpost.js";


//import 'leaflet/dist/leaflet.css';
// import 'leaflet/dist/leaflet.css';
import { Icon } from 'leaflet';

delete Icon.Default.prototype._getIconUrl;
Icon.Default.mergeOptions({
  iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
  iconUrl: require('leaflet/dist/images/marker-icon.png'),
  shadowUrl: require('leaflet/dist/images/marker-shadow.png'),
});


Vue.use(ajaxpost);


Vue.component('l-map', LMap);
Vue.component('l-tile-layer', LTileLayer);
Vue.component('l-marker', LMarker);
Vue.component('l-popup', LPopup);
Vue.component('l-tooltip', LTooltip);
Vue.component('l-circle', LCircle);
Vue.component('l-circle-marker', LCircleMarker);
Vue.component('l-control-scale', LControlScale);
Vue.component('l-icon', LIcon);

Vue.config.productionTip = false
// const vuetify = new Vuetify({
//   lang: {
//     locales: { ru },
//     current: 'ru',
//   },
//   icons: {
//     iconfont: "mdi"
//   },
// });

new Vue({
  vuetify,
  router,
  render: h => h(App)
}).$mount('#app')
