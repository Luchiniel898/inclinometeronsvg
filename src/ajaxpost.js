

function toGET(url, data, token) {
    let link = url.split("#");
    link = link[0].split("?");
    let param = link.slice(1);
    link = link[0];
    param = param.map(i => i.split("&").map(i => i.split("=")))[0] || [];
    if (token) {
        data.auth = token;
    }
    for (let i in data) {
        if (Array.isArray(data[i])) {
            for (let j in data[i]) {
                param.push([i + "[]", data[i][j]].map(encodeURI));
            }
        } else {
            param.push([i, data[i]].map(encodeURI))
        }
    }
    param = param.map(i => i.join("=")).join("&");

    return [link, param].filter(i => i).join("?");
}

function toPOST(data, token) {
    let form = new FormData();
    if (token) {
        data.auth = token;
    }
    for (let i in data) {
        if (Array.isArray(data[i])) {
            for (let j in data[i]) {
                form.append(i + "[]", data[i][j]);
            }
        } else {
            form.append(i, data[i]);
        }
    }
    return form;
}

function ajax(url, options) {
    let token = localStorage.auth || "";
    return new Promise((resolve, reject) => {
        fetch(toGET(url, options.get || {}, (options.method == "POST" || options.post) ? undefined : token), {
            credentials: "include",
            headers: {
                accept: "application/json"
            },
            body: options.post ? toPOST(options.post, token) : null,
            method: (options.method == "POST" || options.post) ? "POST" : "GET",
            mode: "cors"
        }).then(res => res.text()).then(text => {
            try {
                let json = JSON.parse(text.trim());
                if(json && json.user && json.user.token){
                    /*localStorage.token = */
                    token = json.user.token;
                }
                resolve(json);
            }catch (e) {
                reject(e);
            }
        }).catch(reject);
    });
}
let importedJS = [];
function importJS(src){
    if(importedJS.indexOf(src) == 0){
        return Promise.resolve();
    }else {
        return new Promise((resolve, reject) => {
            importedJS.push(src);
            let s = document.createElement("SCRIPT");
            s.onload = resolve;
            s.onerror = reject;
            s.src = src;
            document.head.appendChild(s);
        });
    }
}

export default {
    install(Vue) {

        /**
         * POST запрос
         *
         * @param url - адрес запроса
         * @param postParam - параметры запроса
         * @return Promise
         */
        Vue.prototype.$post = (url, postParam) => ajax(url, {method: "POST", post: postParam});

        /**
         * GET запрос
         *
         * @param url - адрес запроса
         * @param getParam - параметры запроса
         * @return Promise
         */
        Vue.prototype.$get = (url, getParam) => ajax(url, {method: "GET", get: getParam});

        /**
         * AJAX запрос
         * @param url - адрес запроса
         * @param options - параметры запроса
         * @return Promise
         */
        Vue.prototype.$ajax = ajax;
        Vue.prototype.$importJS = importJS;
    }
};
