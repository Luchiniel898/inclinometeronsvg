export default class Wialon {
    constructor(map, url) {
        this.url=url;
        this.map = map;
        this.markers = {};
        this.cars = [];
        this.tile_l = '';
        this.lay = {};
        this.mark = {};
        this.pos = {};
        this.layers = {};
        this.offset = 16;
    }

    loginWln(token) {
        let user = wialon.core.Session.getInstance().getCurrUser();
        if (user === null) {
            wialon.core.Session.getInstance().initSession("https://hst-api.wialon.com"); // init session
            wialon.core.Session.getInstance().loginToken(token, "",
                (code) =>{
                    { // login callback
                        if (code) {
                            console.log(wialon.core.Errors.getErrorText(code));
                            return;
                        }
                        this.init_map(7, this.map);
                        this.initTrack();
                    }
                });
        }
    };

    login(token) {
        return new Promise((resolve, reject) => {
            let user = wialon.core.Session.getInstance().getCurrUser();

            if (user === null) {
                wialon.core.Session.getInstance().initSession(this.url); // init session
                wialon.core.Session.getInstance().loginToken(token, "",
                    (code)=> {

                        { // login callback

                            if (code) {
                                reject(wialon.core.Errors.getErrorText(code));
                                return;
                            }
                            let y = "Logged successfully";
                            resolve(y);
                        }
                    });
            }
        })

    };

    getAutoWialon() {
        return new Promise((resolve, reject) => {
            let sess = wialon.core.Session.getInstance(); // get instance of current Session
            // flags to specify what kind of data should be returned
            let flags = wialon.item.Item.dataFlag.base | wialon.item.Unit.dataFlag.restricted;
            sess.loadLibrary("itemIcon"); // load Icon Library
            sess.updateDataFlags( // load items to current session
                [{type: "type", data: "avl_unit", flags: flags, mode: 0}], // Items specification
                (code, func)=> { // updateDataFlags callback
                    if (code) {
                        reject(wialon.core.Errors.getErrorText(code));

                    }
                    let units = sess.getItems("avl_unit");
                    resolve(units);

                }
            )
        })
    }

    getHardware() {
        return new Promise((resolve, reject) => {
            let vy = [];
            wialon.core.Session.getInstance().getHwTypes({
                "filterType": "name",
                "filterValue": "name",
                "includeType": 1,
                "ignoreRename": 1
            },  (i, val)=> {
                //console.log (val.id +' '+val.name);
                vy = val;
                if (vy !== '') {
                    resolve(vy);
                } else {
                    reject(new Error('Что-то пошло не так'));
                }
            })

        });
    }
    logout() {
        this.delObserver();
        this.delete_tracks();
        let user = wialon.core.Session.getInstance().getCurrUser(); // get current user
        this.tile_l = '';
        if (!user) {
            console.log("You are not logged, click 'login' button");
            return;
        }
        wialon.core.Session.getInstance().logout( // if user exist - logout
            (code) =>{ // logout callback
                if (code) console.log(wialon.core.Errors.getErrorText(code)); // logout failed, print error
                else console.log("Logout successfully"); // logout suceed
            }
        );

    };

    addCarWialon(brand, typeHardware) {
        wialon.core.Session.getInstance().createUnit(wialon.core.Session.getInstance().getCurrUser(), brand, typeHardware, 1, function (x, val) {
        });

    };

    delCarWialon(idCarWialone, funcexec) {
        getDataWialone("https://hst-api.wialon.com/wialon/ajax.html?svc=item/delete_item&sid=" + wialon.core.Session.getInstance().getId(), "params={\"itemId\":" + idCarWialone + "}", funcexec);
    };

    addUe2(idCarWialone, typeHardware, UE2, funcexec) {
        getDataWialone("https://hst-api.wialon.com/wialon/ajax.html?svc=unit/update_device_type&sid=" + wialon.core.Session.getInstance().getId(), "params={\"itemId\":" + idCarWialone + ",\"deviceTypeId\":" + typeHardware + ",\"uniqueId\":" + UE2 + "}", funcexec);
    };

    getSelectedUnitInfo() { // print information about selected Unit

        let val = $("#units").val(); // get selected unit id
        if (!val) return; // exit if no unit selected

        let unit = wialon.core.Session.getInstance().getItem(val); // get unit by id
        if (!unit) {
            console.log("Unit not found");
            return;
        } // exit if unit not found

        // construct message with unit information
        let text = "<div>'" + unit.getName() + "' selected. "; // get unit name
        let icon = unit.getIconUrl(32); // get unit Icon url
        if (icon) text = "<img class='icon' src='" + icon + "' alt='icon'/>" + text; // add icon to message
        let pos = unit.getPosition(); // get unit position
        if (pos) { // check if position data exists
            let time = wialon.util.DateTime.formatTime(pos.t);
            text += "<b>Last message</b> " + time + "<br/>" + // add last message time
                "<b>Position</b> " + pos.x + ", " + pos.y + "<br/>" + // add info about unit position
                "<b>Speed</b> " + pos.s; // add info about unit speed
            // try to find unit location using coordinates
            wialon.util.Gis.getLocations([{lon: pos.x, lat: pos.y}],  (code, address)=> {
                if (code) {
                    console.log(wialon.core.Errors.getErrorText(code));
                    return;
                } // exit if error code
                console.log(text + "<br/><b>Location of unit</b>: " + address + "</div>"); // print message to log
            });
        } else // position data not exists, print message
            console.log(text + "<br/><b>Location of unit</b>: Unknown</div>");
    };

    init_map(offset) {
        let sess = wialon.core.Session.getInstance(); // get instance of current Session
        let locale = {};
        locale.formatDate = "%Y-%m-%E %H:%M:%S";
        locale.flags = 0;
        let localeflag = 60 * 60 * offset;
        sess.getRenderer().setLocale(localeflag, "ru", locale, null);
    };

    update_renderer() {
        let sess = wialon.core.Session.getInstance(),
            renderer = sess.getRenderer();
        if (this.tile_l)
            this.tile_l.sess.getBaseUrl() + "/adfurl" + renderer.getVersion() + "/avl_render/{x}_{y}_{z}/" + sess.getId() + ".png"; // update url-mask in tile-layer
    };

    /**
     *
     * */
    initTrack() { // Execute after login succeed
        return new Promise((resolve, reject) => {
            let arr=[];
            let sess = wialon.core.Session.getInstance(), // get instance of current Session
                flags = wialon.item.Item.dataFlag.base | wialon.item.Unit.dataFlag.lastMessage, // specify what kind of data should be returned
                renderer = wialon.core.Session.getInstance().getRenderer();

            renderer.addListener("changeVersion", this.update_renderer);
            sess.loadLibrary("itemIcon"); // load Icon Library

            sess.updateDataFlags( // load items to current session
                [{type: "type", data: "avl_unit", flags: flags, mode: 0}], // Items specification
                (code) => { // updateDataFlags callback
                    if (code) {
                        reject(wialon.core.Errors.getErrorText(code));
                        return;
                    } // exit if error code;
                    let units = wialon.core.Session.getInstance().getItems("avl_unit"); // get loaded 'avl_resource's items


                    for (let i = 0; i < units.length; i++) {
                        let ind = units[i].getId();
                        arr.push(ind)

                        let pos=units[i].getPosition();
                        let item=  sess.getItem(ind);
                    }

                    if (!units || !units.length) {
                        console.log("No units found");
                        return;
                    } // check if units found
                    resolve(arr);
                })


        });
    }

    show_track(from, to, idCar, lCallback) {

        this.delete_tracks();
        this.cars = [];
        this.cars.push(idCar);

        let sess = wialon.core.Session.getInstance(),// get instance of current Sessio
            renderer = sess.getRenderer(),
            unit = sess.getItem(idCar), // get unit by id
            color = "0000ff"; // track color

        console.log('sess', sess);
        console.log('unit', unit);

        if (!unit) return; // exit if no unit

        // check the existence info in table of such track
        if (document.getElementById(idCar)) {
            console.log("You already have this track.");
            return;
        }

        let pos = unit.getPosition(); // get unit position
        if (!pos) return; // exit if no position

        // callback is performed, when messages are ready and layer is formed

        let callback = qx.lang.Function.bind( (code, layer) =>{
            if (code) {
                return;
            } // exit if error code
            if (layer) {
                // console.log (56);
                let layer_bounds = layer.getBounds(); // fetch layer bounds
                if (!layer_bounds || layer_bounds.length != 4 || (!layer_bounds[0] && !layer_bounds[1] && !layer_bounds[2] && !layer_bounds[3])) // check all bounds terms
                    return;
                // if map existence, then add tile-layer and marker on it
                if (this.map) {
                    //prepare bounds object for map
                    if (lCallback !== undefined) {
                        lCallback(layer_bounds);
                    }
                    let bounds = new L.LatLngBounds(
                        L.latLng(layer_bounds[0], layer_bounds[1]),
                        L.latLng(layer_bounds[2], layer_bounds[3])
                    );
                    // this.map.fitBounds(bounds); // get center and zoom
                    if (!this.tile_l) {
                        this.tile_l = L.tileLayer(sess.getBaseUrl() + "/adfurl" + renderer.getVersion() + "/avl_render/{x}_{y}_{z}/" + sess.getId() + ".png", {
                            zoomReverse: true,
                            zoomOffset: -1
                        }).addTo(this.map);
                    } else {
                        this.tile_l.setUrl(sess.getBaseUrl() + "/adfurl" + renderer.getVersion() + "/avl_render/{x}_{y}_{z}/" + sess.getId() + ".png");
                    }
                    renderer.getVersion().log;
                    // push this layer in global container
                    this.layers[idCar] = layer;
                    // get icon
                    // let icon = L.icon({iconUrl: unit.getIconUrl(24)});
                    // //create or get marker object and add icon in it
                    // let marker = L.marker({lat: pos.y, lng: pos.x}, {icon: icon}).addTo(this.map);
                    // marker.setLatLng({lat: pos.y, lng: pos.x}); // icon position on map
                    // marker.setIcon(icon); // set icon object in marker
                    // this.markers[idCar] = marker;
                }
            }
        });
        // query params
        let params = {
            "layerName": "route_unit_" + idCar, // layer name
            "itemId": idCar, // ID of unit which messages will be requested
            "timeFrom": from, //interval beginning
            "timeTo": to, // interval end
            "tripDetector": 0, //use trip detector: 0 - no, 1 - yes
            "trackColor": color, //track color in ARGB format (A - alpha channel or transparency level)
            "trackWidth": 2, // track line width in pixels
            "arrows": 1, //show course of movement arrows: 0 - no, 1 - yes
            "points": 1, // show points at places where messages were received: 0 - no, 1 - yes
            "pointColor": color, // points color
            "annotations": 0, //show annotations for points: 0 - no, 1 - yes
            "flags": 8352
        };

        renderer.createMessagesLayer(params, callback);
        //logout();
    };

    delete_track(idCar) {
        let sess = wialon.core.Session.getInstance();
        let renderer = sess.getRenderer();
        renderer.removeLayer(this.layers[idCar]);
        delete this.layers[idCar];
        this.map.removeLayer(this.markers[[idCar]]);
        delete this.markers[idCar];
    }

    delete_tracks() {
        let sess = wialon.core.Session.getInstance();
        let renderer = sess.getRenderer();
        for (let k in this.layers) {
            if (this.layers.hasOwnProperty(k)) {
                renderer.removeLayer(this.layers[k]);
                delete this.layers[k];
            }
        }
        for (let k in this.markers) {
            if (this.markers.hasOwnProperty(k)) {
                this.map.removeLayer(this.markers[k]);
                delete this.markers[k];
            }
        }
        this.cars.forEach(i => {
        })
    }
}